const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
  entry: `./_js/main.js`,
  output: {
    path: `${__dirname}/_public/js`,
    filename: "main.js"
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new WebpackNotifierPlugin({
      alwaysNotify: true
    }),
  ]
};
