import "./smoothScroll";

/*
 *
 * ----------------------------------------------- */
jQuery(function ($) {
  $('.page-top').on('click', function (event) {
    $.smoothScroll({
      easing: 'swing',
      speed: 400
    });

    return false;
  });
});
