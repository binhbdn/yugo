/*
 * menu-trigger btn
 * ----------------------------------------------- */
(function () {
  var $menuTrigger = $('.menu-trigger');

  $menuTrigger.on('click', function (e) {
    e.preventDefault();
  });

  $.each($menuTrigger, function (i) {
    var $_this = $(this);
    var id = $_this.attr('data-target');

    $(id).on('show.bs.collapse hide.bs.collapse', function (e) {
      if ('#' + $(e.target).attr('id') === id) {
        $_this.toggleClass('active');
      }
    });
  });
})();
