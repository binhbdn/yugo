// MEMO: import this for IE11.
// MEMO: Swiper 5.4.5 works on IE11.
import Swiper from '../../node_modules/swiper/js/swiper';

var swiper = new Swiper('.swiper-container', {
  autoplay: {
    delay: 2500,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
