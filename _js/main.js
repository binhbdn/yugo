import 'bootstrap-sass';
import 'jquery-match-height';

import "./modules/webfont";
import "./modules/page-top";
import "./modules/jqueryvalidation";
import {menuTrigger} from "./modules/menu-trigger";
import "./modules/bs.tab";
import "./modules/js-header";
import "./modules/js-footer";
import "./modules/waypoints";
import "./modules/dropdown-hover";
import "./modules/swiper-fv";

// import ScrollMagic from 'scrollmagic';
